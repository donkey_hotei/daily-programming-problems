#!/usr/bin/python

# Nov 29th, 2019
# Daily Coding Problems: Day One

# Problem:
# Given a stack of N elements, interleave the first half of the stack
# with the second half reversed using one other queue.

# For example, if the stack is [1, 2, 3, 4, 5], it should become [1, 5, 2, 4, 3].

if __name__ == "__main__":
    pass
