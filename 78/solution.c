#include<stdlib.h>
#include<stdio.h>
#include<assert.h>

typedef struct Node {
    struct Node * next;
    int value;
} Node;

void move_node(struct Node ** dst_reference, struct Node ** src_reference) {
    struct Node * fresh_node = *src_reference;

    *src_reference   = fresh_node->next;
    fresh_node->next = *dst_reference;
    *dst_reference   = fresh_node;
}

void add_node(Node ** head, int value) {
    Node * new_node = (Node *)malloc(sizeof(Node));
    new_node->value = value;

    if (!(*head)) {
        new_node->next = NULL;
        (*head) = new_node;
    } else {
        new_node->next = (*head);
        (*head) = new_node;
    }

    return;
}

int main(int argc, char ** argv) {
    Node * head = NULL;

    int i;
    for (i = 0; i <= 10; i++)
        add_node(&head, i);
}