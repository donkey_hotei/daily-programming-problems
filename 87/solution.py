import networkx as nx

class CardinalGraph(nx.MultiDiGraph):
    def __init__(self):
        super(CardinalGraph, self).__init__(self)

    def cardinal_directions(self):
        return  ["N", "E", "W", "S"]

    def diagonal_directions(self):
        return { "NW": ["N", "W"],
                 "NE": ["N", "E"],
                 "SE": ["S", "E"],
                 "SW": ["S", "W"] }

    def add_edge(self, u_of_edge, v_of_edge, **attr):
        """ Adds two edges between u and v, one u
        """
        if "weight" not in attr:
            raise TypeError("a 'weight' parameter is required")

        direction = attr["weight"]

        if direction not in self.cardinal_directions() and \
           direction not in self.diagonal_directions():
            raise TypeError("weight must be on of the cardinal directions")

        if direction in self.diagonal_directions():
            for component_direction in self.diagonal_directions()[direction]:
                super(CardinalGraph, self).add_edge(
                        u_of_edge, v_of_edge,
                        weight=self.opposite_of(component_direction))
                super(CardinalGraph, self).add_edge(
                        u_of_edge, v_of_edge,
                        weight=component_direction)
        else:
            super(CardinalGraph, self).add_edge(
                    u_of_edge, v_of_edge,
                    weight=self.opposite_of(direction))
            super(CardinalGraph, self).add_edge(
                    u_of_edge, v_of_edge, weight=direction)

    def add_opposite_edge(self, u_of_edge, v_of_edge, weight=None):
        if weight is None:
            raise TypeError("a 'weight' parameter is required")

    def opposite_of(self, direction):
        if direction == "N": 
            return "S"
        elif direction == "S": 
            return "N"
        elif direction == "E": 
            return "W"
        elif direction == "W": 
            return "E"
        else:
            raise TypeError("%s is not a valid direction" % direction)

    def is_valid(self):
        for node in self.nodes():
            for edge in nx.dfs_edges(self, node):
                import ipdb; ipdb.set_trace()


def main():
    g = CardinalGraph()
    g.add_edge("A", "B", weight="N")
    g.add_edge("B", "C", weight="NE")
    g.add_edge("C", "A", weight="N")
    print g.is_valid()

if __name__ == "__main__":
    main()