#!/usr/bin/ruby

def puts_clockwise_spiral(array)
  index, x, y = [0, 0, 0]

  loop do
    l = 0
    if index % 2 == 0
      # horizontal scan
      l = array[0].length
    else
      # vertical scan
      l = array.length - (index / 2)
    end

    return if l == 0

    x_dir = 0
    y_dir = 0

    case index % 4
    when 0 then y_dir = 1
    when 1 then x_dir = 1
    when 2 then y_dir = -1
    when 3 then x_dir = -1
    end

    (0..array.length).each do |i|
      puts array[x + (i + 1) * x_dir][y + (i + 1) * y_dir]
    end

    x = x + l * x_dir
    y = y + 1 * y_dir

    index += 1
  end
end

def main
  a = [[1, 2, 3],
       [4, 5, 6],
       [7, 8, 9]]
  # 1 2 3 6 9 8 7 4 5
  puts_clockwise_spiral(a)
end

main