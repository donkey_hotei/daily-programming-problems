#!/usr/bin/ruby

class Graph
  attr_reader :graph, :m, :n

  def initialize(graph)
    @graph = graph
    @m, @n = graph[0].length, graph.length
  end

  def safe?(x, y)
    return (0 <= x && x < m &&
            0 <= y && y < n &&
            (graph[x]&[y] &&
             !visited[x][y]))
  end

  def dfs(x, y)
    dx = [-1, 0, 1, -1, 0, 1, -1, 0,  1]
    dy = [ 1, 1, 1,  0, 0, 0, -1, 0, -1]

    visited[x][y] = true

    for i in (0..8)
      x_dir = x + dx[i]
      y_dir = y + dy[i]

      next unless safe?(x_dir, y_dir)

      dfs(x_dir, y_dir)
    end
  end

  def visited
    @visited ||=
      Array.new(m, false) { Array.new(n, false) }
  end

  def island_count
    count = 0
    for x in (0...m)
      for y in (0...n)
        next if visited[x][y]
        count += 1
        dfs(x, y)
      end
    end
    count
  end
end

def main
  graph = [[0, 0, 1, 0, 0, 0],
           [0, 1, 1, 0, 0, 1],
           [1, 0, 0, 0, 1, 1],
           [0, 0, 0, 0, 0, 0],
           [1, 0, 1, 0, 1, 0]]
  g = Graph.new(graph)
  puts g.island_count
end

main
