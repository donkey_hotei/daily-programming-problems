#!/usr/bin/python

def switch(x, y, b):
    return (x * b) | (y * (1 - b))

def main():
    x = 42
    y = 666

    print(switch(x, y, 1))
    print(switch(x, y, 0))

if __name__ == "__main__":
    main()