#include<stdlib.h>
#include<stdio.h>

/*
 * If b is 1, then return x.
 * If b is 0, then return y.
 */
int x_or_y(int x, int y, int b) {
    return (((b << 31) >> 31) & x) | (((!b << 31) >> 31) & y);
}