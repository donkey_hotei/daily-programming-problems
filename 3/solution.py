#!/usr/bin/python

class Node:
    def __init__(self, data, left=None, right=None):
        self.left  = left
        self.right = right
        self.data  = data

def serialize(root):
    s = ""

    if root is None:
        s += "#"
        return s

    s += str(root.data)
    s += serialize(root.left)
    s += serialize(root.right)

    return s



if __name__ == "__main__":
    tree = Node(1,
                    Node(2,
                            Node(6)
                        ),
                    Node(5,
                            Node(8),
                            Node(11)
                        )
                )

    print serialize(tree)
