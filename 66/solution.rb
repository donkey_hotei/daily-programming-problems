#!/usr/bin/ruby

#
# Problem: Assume you have access to a function toss_biased() which returns 0 or 1
#          with a probability that's not uniform. Write a function that simulates
#          an unbiased coin toss.


def toss_biased
  1
end

def toss_unbiased
  # flip twice, if 1 then 0 return 0, if 0 then 1 return 1
  # otherwise, flip twice again until one of the above occurs.
  flip_results = [toss_biased, toss_biased]

  loop do
    break unless flip_results.reduce(:==)
    flip_results = [toss_biased, toss_biased]
  end

  case flip_results[0]
  when 0 then return 1
  when 1 then return 0
  end
end