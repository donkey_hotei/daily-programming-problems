#!/usr/bin/ruby
require "set"

def randrange(lo, hi, grouping=1)
  (lo...hi).step(grouping).to_a.sample
end

def process_list(n, l)
  ((0...n).inject(Set.new) { |s, e| s << e } - Set.new(l)).to_a
end

def random_number_excluding_list(n, l)
  nums_list = process_list(n, l)
  nums_list[randrange(0, nums_list.length)]
end

def main
  puts random_number_excluding_list(4, [1, 2, 5])
end

main