#!/usr/bin/ruby

def almost_monotone_increasing?(array)
  k = nil
  n = array.length

  (0...n - 1).each do |i|
    if array[i] > array[i + 1]
      if k.nil?
        k = i
      else
        return false
      end
    end
  end

  return (k.nil? ||
          k == 0 ||
          k == n - 2 ||
          array[k - 1] < array[k + 1] ||
          array[k] < array[k + 2])
end

def main
  puts almost_monotone_increasing?([1, 2, 3])
  puts almost_monotone_increasing?([4, 5, 6, 1, 2, 3])
end

main
