#!/usr/bin/python

# Problem: Given an array of integers, write a function to determine
#          whether the array could become non-decreasing by modifying
#          at most 1 element.
#

def is_non_decreasable(arr):
    count = 0

    for i in range(1, len(arr)):
        print "%d > %d ?" % (arr[i - 1], arr[i])
        if arr[i - 1] > arr[i]:
            count += 1

    return count <= 1

def main():
    a = [10, 5, 7]
    b = [10, 5, 1]
    c = [1, 2, 3]
    d = [4, 5, 6, 1, 2, 3]


    # print(is_non_decreasable(a)) # => True
    # print(is_non_decreasable(b)) # => False
    # print(is_non_decreasable(c)) # => True
    print(is_non_decreasable(d)) # => False

if __name__ == "__main__":
    main()