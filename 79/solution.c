#include<stdlib.h>
#include<stdio.h>
#include<stdbool.h>

/*
 * Returns true if A can be made monotone increasing
 * by decreasing at most one of it's elements.
 */
bool almost_monotone_increasing(int A[], int n) {
    int * k = NULL;

    int i;
    for (i = 0; i < n - 1; i++) {
        if (A[i] > A[i + 1]) {
            if (k != NULL)
                return false;
            k = &i;
        }
    }
    return (*k == 0     ||
           *k == n - 2 ||
           A[*k - 1] <= A[*k + 1]
           || A[*k]  <= A[*k + 2]);
}

int main( ) {
    int a[3] = { 1, 2, 3 };
    int b[6] = { 4, 5, 6, 1, 2, 3 };

    if (almost_monotone_increasing(a, 3)) {
        printf("Almost monotone increasing!\n");
    } else {
        printf("Not almost monotone increasing.\n");
    }
    if (almost_monotone_increasing(b, 6)) {
        printf("Almost monotone increasing!\n");
    } else {
        printf("Not almost monotone increasing.\n");
    }
}