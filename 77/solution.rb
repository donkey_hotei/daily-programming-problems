#!/usr/bin/ruby

def overlapping?(i1, i2)
  i1[0] <= i2[1] ? true : false
end

def merge_overlapping(intervals)
  intervals = intervals.sort
  stack     = []
  intervals.each do |interval|
    if stack.empty?
      stack.push(interval)
      next
    elsif overlapping?(interval, stack[-1])
      stack[-1][1] = [interval[1], stack[-1][1]].max
    else
      stack.push(interval)
    end
  end

  stack
end

def main
  intervals = [[1, 3], [5, 8], [4, 10], [20, 25]]
  merged_intervals = merge_overlapping(intervals)
  puts merged_intervals.inspect
end

main