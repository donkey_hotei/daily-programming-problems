#!/usr/bin/ruby

# Problem: Given a hashmap with a key 'courseId' and a list of 'courseIds', which
#          represent that the prerequisite of 'courseId' is 'courseIds'. Return a
#          sorted ordering of courses s.t. we can finish all of the courses.
#

def khan(g)
  result = []
  next_n = []

  g.keys.each do |node|
    next_n << node if g[node].empty?
  end

  while !next_n.empty?
    node = next_n.pop
    result << node

    dependencies = g.keys.select do |n| 
      g[n].include?(node)
    end

    dependencies.each do |dependency|
      g[dependency].delete(node)
      next_n << dependency if g[dependency].empty?
    end
  end

  result
end

def main
  g = { "CSC300" => [ "CSC100", "CSC200" ],
        "CSC200" => [ "CSC100" ],
        "CSC100" => [] }

  dependencies = khan(g)
  puts dependencies.inspect
end

main