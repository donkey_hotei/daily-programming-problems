#!/usr/bin/python
from string import digits
from itertools import combinations

PHONE_WORDS = {
        1: [],
        2: ["a", "b", "c"],
        3: ["d", "e", "f"],
        4: ["g", "h", "i"],
        5: ["j", "k", "l"],
        6: ["m", "n", "o"],
        7: ["p", "q", "r", "s"],
        8: ["t", "u", "v"],
        9: ["w", "x", "y", "z"],
        0: []
}

def assert_is_digit(n):
    all(c in digits for c in str(n))

def phone_word_combinations(number):
    assert_is_digit(number)
    word_set = []
    for digit in str(number):
        word_set.extend(PHONE_WORDS[int(digit)])
    word_combinations = combinations(word_set, len(str(number)))

    return word_combinations

def main():
    n = 23
    words = phone_word_combinations(n)
    for word in words:
        print("".join(word))

if __name__ == "__main__":
    main()