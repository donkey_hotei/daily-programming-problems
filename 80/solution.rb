#!/usr/bin/ruby

# Problem: Given the root of a binary tree, return the deepest node.

class Node
  attr_reader :value

  def initialize(value, left=nil, right=nil)
    @value, @left, @right = value, left, right
  end

  def children
    [@left, @right].compact
  end

  def height
    return 0 if children.empty?
    children.collect { |c| c.height + 1 }.max
  end

  def deepest_node
    return [self] if height == 0  # base case: leaf node
    chosen_children = children.select do |child|
      child.height == height - 1  # go for depth
    end
    chosen_children.collect do |child|
      child.deepest_node # recurse down the tree
    end.flatten
  end
end

def main
  tree = Node.new("A",
                  Node.new("B"),
                  Node.new("C",
                           Node.new("D")
                          )
                 )
  puts tree.deepest_node[0].value
end

main
