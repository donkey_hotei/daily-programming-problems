#!/usr/bin/ruby
require "set"

class Counter < Hash
  def initialize(obj)
    # initialize values to zero
    super(0)

    if obj.is_a?(String)
      obj.each_char do |x|
        self[x] += 1
      end
    end
    if obj.is_a?(Array)
      obj.each do |x|
        self.x += 1
      end
    end
    if obj.is_a?(Hash)
      obj.each do |k, v|
        self[k] = v
      end
    end
  end

  def most_common(n = nil)
    # sort into ascending order
    s = sort_by { |k, v| -v }
    return n ? s.take(n) : s
  end
end

#
# Naive solution which runs DFS on every path, 
# returning nil if we come across a cycle.
#
# Terribly slow at O(V + E) where V and E are the
# number of vertices and edges repsectively. Since
# we also evaluate that current path each time it is
# O(V * (V + E))
#
class DirectedGraph
  VISTED    = 0
  UNVISITED = 1
  VISITING  = 2

  attr_accessor :tags, :edgelist, :adj

  def initialize(tags, edgelist)
    self.tags = tags
    self.edgelist = edgelist
    self.adj = build_adjacency_list
  end

  def naive_max_path
    maximum_path = 0
    # every path from node v
    (0..tags.length - 1).each do |v|
      # [path_string, visited, current_node]
      stack = [[tags[v], Set.new([v]), v]]

      loop do
        break if stack.empty?

        path_string, visited, current_node = stack.pop

        # count value of current path and update maximum_path if needed
        count = Counter.new(path_string)
        _, path_val = count.most_common(1)[0]
        maximum_path = [maximum_path || 0, path_val || 0].max

        adj[current_node].each do |neighbor|
          return nil if visited.include?(neighbor)
          # take current path_string, and append the tag of
          # the neighbor of the current_node, adding the
          # neighbor to the set of visited nodes, and then
          # the neightbor itself (to be popped off next).
          stack << ([
            path_string + tags[neighbor],
            visited | [neighbor],
            neighbor
          ])
        end
      end
    end
    maximum_path
  end

  # Instead of recomputing each path every iteration,
  # we only need to increment that one character whose
  # count would change. Also, for our tags we're only
  # using an alphabet of 26 characters, a fixed number
  # of potential values to contributes to the longest
  # chain.
  #
  # Using our cache we keep track of the path with the
  # largest value using a recurence:
  #   1. When we get to a node v, we'll do a DFS on all
  #      it's neighbors.
  #   2. Then, A[v][j] will be the maximum of all A[neightbor][j]
  #      for all it's neighbors.
  #   4. Count the current node as well by incrementing A[v][current_char]
  #      by one, where current_char is the current node's tag.
  #
  # Using DFS as in the naive solution, to search the graph as well
  # as determining if there is a cycle.
  def max_path
    def dfs(v)
      state[v] = VISITING

      adj[v].each do |neighbor|
        # do we have a cycle?
        return true if state[neighbor] == VISITING

        dfs(neigbor)  # updates the cache

        (0..26).each do |i|
          cache[v][i] = cache[neighbor][i]
        end
      end

      current_char = tags[v].ord - "A".ord
      cache[v][current_char] += 1
      state[v] = VISITED
    end

    # Run DFS on the graph
    (0..tags.length - 1).each do |v|
      if state[v] == UNVISITED
        has_cycle = dfs(v)
        return nil if has_cycle
      end
    end

    dp.each do |node|
      node.each do |v|
        v
      end.max
    end.max
  end

  def inspect
    adj.inspect
  end

  private

  def cache
    # A square matrix of size length(tags) where
    # A[i][j] contains the maximum value of the
    # path that can be made from a character i
    # (where i is the index into the alphabet).
    @cache ||= (0..tags.length - 1).map do|_|
      (0..26).each { |_| 0 }
    end
  end

  def state
    @state = (0..tags.length - 1).inject({}) do |h, v|
      h[v] = UNVISITED
    end
  end

  def build_adjacency_list
    adj = (0..tags.length - 1).map { |x| [] }
    edgelist.each do |u, v|
      adj[u] << v
    end
    adj
  end
end

def main
  s = "ABACA"
  e = [[0, 1], [0, 2], [2, 3], [3, 4]]
  graph = DirectedGraph.new(s, e)
  puts graph.naive_max_path  # O(V * (V + E))
  puts graph.max_path        # O(V + E)
end

main
