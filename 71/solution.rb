#!/usr/bin/ruby

# Problem: Given a function rand7 that returns a value between [0, 7)
#          write a function rand5 that returns a value between [0, 5).

# The harder problem is going from rand5 to rand7, let's start there.
def rand2
  # Called until we hit a number less than 4,
  # set that number mod 2. Which will equal 0 
  # if that number was 0 or 2, 1 otherwise.
  n = rand5
  return rand2 if n == 4
  n % 2
end

def rand7
  # A number between 0 and 7 will be 3 bits
  # so we randomly generate each bit using
  # the rand2 function, scaling each value
  # by a power of 2.
  n = rand2 * 4 + rand2 * 2 + rand2
  return rand7 if n == 7
  n
end

# To go from rand7 to rand5 we merely need to
# keep calling rand7 until we get a value that
# is between 0 and 5. Chances of hitting those
# values will still be uniform.
def rand5
  n = rand7
  loop do
    break if (0..5).include?(n)

    n = rand7
  end
end
