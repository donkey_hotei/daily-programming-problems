#include<stdlib.h>
#include<stdio.h>
#include<assert.h>

/*
 * Given an unsigned 8-bit integer, swap its even and odd bits.
 * The first and second bit should be swapped, and the third and
 * fourth bit should be swapped, and so on.
 */

unsigned int swap_bits(unsigned int n) {
    unsigned int odd_bits   = n & 0xAAAAAAAA;
    unsigned int even_bits  = n & 0x55555555;

    odd_bits  >>= 1;
    even_bits <<= 1;

    return odd_bits | even_bits;
}


int main() {
    assert(swap_bits(23) == 43);

    return 0;
}
