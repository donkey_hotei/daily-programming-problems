#!/usr/bin/env ruby

#
# Given two strings A and B, return whether or not A can be shifted some
# number of times to get B.
#

def can_be_shifted?(a, b)
  (0..a.length).each do |n|
    return true if a.split("").rotate(n).join == b
  end

  return false
end

class AssertionError < RuntimeError; end

def assert(&block)
  raise AssertionError unless yield
end

def test
  assert { can_be_shifted?("abcde", "cdeab") == true }
  assert { can_be_shifted?("abc", "acb") == false }
  puts "tests pass"
end

test