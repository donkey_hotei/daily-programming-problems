#!/usr/bin/ruby

#
# Problem: Given a list of integers, return the largest product that can be
#          made by multiplying any three integers.
#

def max_triplet_product(arr)
  arr.sort!
  a, b, c = arr.last(3)
  x, z    = arr.first(2)

  abc_prod = a * b * c
  cxz_prod = c * x * z

  if abc_prod > cxz_prod
    abc_prod
  else
    cxz_prod
  end
end

def main
  a = [-10, -10, 5, 2]
  puts max_triplet_product(a)
end

main
