#!/usr/bin/ruby

# Problem: Given a string of parenthesis, write a program to count the number
#          of parens that would need to be deleted to make it balanced.
#
class Stack
  def initialize(elements = nil)
    @elements = elements || []
  end

  def empty?
    @elements.empty?
  end

  def count
    @elements.length
  end

  def top
    @elements.last
  end

  def push(element)
    @elements.push(element)
  end

  def pop
    @elements.pop
  end
end

class ParenValidator
  def initialize(string)
    @string = string
  end

  def count_deletions_to_balance
    stack = Stack.new
    count = 0

    @string.each_char do |c|
      case c
      when ")"
        if stack.top != "("
          count += 1
        end
        stack.push(c)
      when "("
        if @string.length == 1
          count += 1
        end
        stack.push(c)
      else
        stack.push(c)
      end
    end

    count
  end
end

def main
  invalid_string_one = ")("
  invalid_string_two = "()())()"

  paren_validator = ParenValidator.new(invalid_string_one)
  puts paren_validator.count_deletions_to_balance
  paren_validator = ParenValidator.new(invalid_string_two)
  puts paren_validator.count_deletions_to_balance

end

main
