#!/usr/bin/env python

#
# Given a word W and a string S, find all starting indices in S which are
# anagrams of W.
#


def is_anagram(s1, s2):
    return sorted(list(s1)) == sorted(list(s2))


def can_find_anagram(string, word, start):
    """Checks if a substring (starting at a start index) of a string
    is an anagram of a word"""
    end = start + len(word)

    if end > string:
        return False

    if is_anagram(string[start:end], word):
        return True

    return False


def anagram_starts(string, word):
    """Finds all starting indices of anagrams of word in a string."""
    return [start_index
            for start_index in range(len(string))
            if can_find_anagram(string, word, start_index)]


def test():
    assert anagram_starts("abxaba", "ab") == [0, 3, 4]
    return "tests pass"


if __name__ == "__main__":
    print(test())
