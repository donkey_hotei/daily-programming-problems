#include<stdio.h>
#include<stdlib.h>

/*
 * Problem: Given a singly-linked list, write a function to reverse it in-place.
 *
 * The trick here is fairly simple. We maintain three pointers, one pointing at
 * the previous node, one pointing at the current node, and one pointing at the
 * next node (from the current). Initially, the previous node will be NULL,
 * the current node the head of the singly-linked list, and the next the node
 * following the current. We then simply set the 
 */

/*
 * Singly Linked-List
 */
typedef struct Node {
    struct Node * next;
    int value;
} Node;

/*
 * Adds a node to a linked-list, if there is no head then
 * the new node becomes the head of a new linked-list.
 */
void add_node(Node ** head, int value) {
    Node * new_node = (Node *)malloc(sizeof(Node));
    new_node->value = value;

    if (!(*head)) {
        new_node->next = NULL;
        (*head) = new_node;
    } else {
        new_node->next = (*head);
        (*head) = new_node;
    }

    return;
}

/*
 * Reverse a linked-list in-place.
 */
void reverse(Node ** head) {
    Node * previous = NULL;
    Node * current  = *head;
    Node * next;

    while (current) {
        next = current->next;
        current->next = previous;
        previous = current;
        current  = next;
    }
    *head = previous;

    return;
}

/*
 * Prints a linked list starting at the head.
 */
void print_list(Node * head) {
    Node * current_node = head;

    while (current_node) {
        printf("[%d]-->", current_node->value);
        current_node = current_node->next;
    }
    printf("[]\n");
}

int main( ) {
    Node * head = NULL;

    int i;
    for (i = 0; i <= 10; i++)
        add_node(&head, i);

    print_list(head);
    reverse(&head);
    print_list(head);

    return 0;
}