#!/usr/bin/ruby

#
# Problem: There is an N by M matrix of zeros. Given N and M
#          write a function to count the number of ways you
#          can get from the top-left corner to the bottom-right
#          corner. You can only move right or down.
#

def number_of_paths(n, m)
  if (n == 1) || (m == 1)
    1
  else
    number_of_paths(n - 1, m) + number_of_paths(n, m - 1)
  end
end

def main
  puts number_of_paths(2, 2) # == 2
  puts number_of_paths(5, 5) # == 70
end

main