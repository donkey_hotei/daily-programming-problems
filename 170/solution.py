#!/usr/bin/python
from collections import defaultdict, deque
from itertools import product


class Graph:
    """
    A graph connecting words to other words based on
    one-letter differences. Used for solving the word
    ladder problem proposed by Lewis Carrol in 1878.
    """
    def __init__(self, words):
        self.adj_list = defaultdict(set)

        #
        # Preprocess list of words into buckets, like:
        #
        #     _ope -> rope, nope, hope, lope
        #     p_pe -> pope, pipe, pape
        #
        buckets = defaultdict(list)
        for word in words:
            for i in range(len(word)):
                bucket = "{}_{}".format(word[:i], word[i + 1:])
                buckets[bucket].append(word)

        #
        # Construct a graph by drawing edges between words
        # (mutual neighbors) that ended up in the same same bucket.
        #
        for bucket, mutual_neighbors in buckets.items():
            for word1, word2 in product(mutual_neighbors, repeat=2):
                if word1 != word2:
                    self.connect(word1, word2)

    def connect(self, u, v):
        self.adj_list[u].add(v)
        self.adj_list[v].add(u)

    def traverse(self, start):
        visited = set()
        queue = deque([[start]])
        while queue:
            path = queue.popleft()
            node = path[-1]
            yield node, path
            for neighbor in self.adj_list[node] - visited:
                visited.add(neighbor)
                queue.append(path + [neighbor])


def main():
    start, end = "dog", "cat"
    words = ["dog", "dot", "dop", "dat", "cat"]
    graph = Graph(words)
    paths = {node: path for node, path in graph.traverse(start)}

    if end not in paths:
        print("No paths from %s to %s found." % (start, end))
    else:
        print("->".join(paths[end]))


if __name__ == "__main__":
    main()