#!/usr/bin/python

def sum_digits(n):
    current_sum = 0

    while n > 0:
        current_sum += n % 10
        n = n // 10
    return current_sum

def perfect_number(n):
    i, curr = 0, 0
    while curr < n:
        i += 1
        if sum_digits(i) == 10:
            curr += 1
    return i

def main():
    for n in range(1, 50):
        print("%d: %s" % (n, perfect_number(n)))

    return

if __name__ == "__main__":
    main()
