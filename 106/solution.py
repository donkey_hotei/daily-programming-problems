#!/usr/bin/env python

"""
Given an array of integers where each number represents the number of hops
you can make. Determine whether you can reach to the last index when starting
from index 0.
"""


def can_hop_to_the_end(hops):
    n = len(hops)
    rabbit = 0

    while rabbit < n - 1:
        if hops[rabbit] == 0 and rabbit < n - 1:
            return False

        rabbit += hops[rabbit]

    return True


def test():
    assert can_hop_to_the_end([2, 0, 1, 0]) is True
    assert can_hop_to_the_end([1, 1, 0, 1]) is False
    print("tests pass")


if __name__ == "__main__":
    print(test())
