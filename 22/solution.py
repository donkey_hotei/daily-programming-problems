#!/usr/bin/python

def solve(text, words):
    indices = sorted([text.find(word) for word in words])

    return [
        text[i:j]
        for i, j in zip(indices, indices[1:] + [len(text)])
    ]

if __name__ == "__main__":
    words = ["quick", "brown", "the", "fox"]
    data = "thequickbrownfox"

    print solve(data, words)