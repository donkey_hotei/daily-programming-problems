#!/usr/bin/ruby
require 'forwardable'
#
# Problem: Implement a stack that has the following methods:
#   1. push(val), which pushes element onto the stack
#   2. pop(), which pops off and returns topmost element of the stack
#   3. max(), returns the maximum value in the stack currently
#
# All of these operations should occur in O(1) time.
#

class Stack
  attr_reader :values
  attr_accessor :max

  extend Forwardable

  def initialize
    @values ||= []
    @max = 0
  end

  def_delegators :values, :pop

  def push(val)
    max = val if val > max
    values.push(val)
  end
end

def main
  stack = Stack.new
  stack.push(1)
  stack.push(2)
  stack.push(3)

  puts stack.pop
end

main