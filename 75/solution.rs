/*
 * Problem: Write program to find the longest increasing
 *          subsequence, which does not have to necessarily
 *          be contiguous.
 */


/*
 * https://en.wikipedia.org/wiki/Longest_increasing_subsequence
 */
fn lis(x: Vec<i32>)-> Vec<i32> {
    let n = x.len();
    // m[j] contains k s.t. x[k] is the longest
    // increasing subsequence of length j ending at x[k].
    let mut m = vec![0; n];
    // p[k] stores the index of the predecessor of x[k]
    // in the longest increasing subsequence ending at x[k].
    let mut p = vec![0; n];
    // l is the legnth of the longest subsequence seen thus far
    let mut l = 0;

    for i in 0..n {
        let mut lo = 1;
        let mut hi = l;

        /*
         * because the sequence x[m[0]] ... x[m[l]]
         * is increasing, for an increasing subsequence
         * of length j > 2 there is a smaller increasing
         * subsequence of length j - 1 (at x[p[m[j]]]) one
         * can perform binary search to find the largest
         * postitive j <= l s.t. x[m[j]] < x[i].
         */
        while lo <= hi {
            let mut mid = (lo + hi) / 2;

            if x[m[mid]] <= x[i] {
                lo = mid + 1;
            } else {
                hi = mid - 1;
            }
        }
        // lo is now greater than the length of the longest
        // prefix of x[i]
        let mut new_l = lo;
        // predecessor of x[i] is the last index of the
        // subsequence of length new_l - 1
        p[i] = m[new_l - 1];
        m[new_l] = i;

        if new_l > l {
            l = new_l;
        }
    }

    // rebulid longest increasing subsequence
    let mut o = vec![0; l];
    let mut k = m[l];
    for i in (0..l).rev() {
        o[i] = x[k];
        k    = p[k];
    }

    o
}

fn main() {
    let v = vec![0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15];
    let o = lis(v);
    println!("{:?}", o);
}