#!/usr/bin/ruby

# https://en.wikipedia.org/wiki/Longest_increasing_subsequence

# Problem: Given an array of numbers find the length
#          of the longest increasing subsequence in the
#          array. This subsequence does not necessarily
#          have to be contiguous.

def lis(s)
  n = s.length

  m = [ 0 ] * (n + 1)
  p = [ 0 ] * n

  l = 0
  (0..n - 1).each do |i|
    # binary search of the largest positive j <= l
    lo = 1
    hi = l

    while lo <= hi do
      mid = ((lo + hi) / 2).ceil

      if s[m[mid]] <=  s[i]
        lo = mid + 1
      else
        hi = mid - 1
      end
    end

    # after the search, lo is 1 more than the length
    # of the longest prefix of s[i]
    new_l = lo
    # the precessor of s[i] is the last index of the
    # subsequence of length new_l - 1
    p[i] = m[new_l - 1]
    m[new_l] = i
    # if we've found a subsequence longer than any we've
    # seen so far, update l
    if new_l > l
      l = new_l
    end
  end

  # Finally reconstruct the longest increasing subsequence
  o = [ 0 ] * l
  k = m[l]
  (l-1).downto(0).each do |i|
    o[i] = s[k]
    k    = p[k]
  end
  o
end


def main
  s = [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15]
  puts s.inspect
  puts lis(s).inspect
  # => [0, 2, 6, 9, 11, 15]
end

main