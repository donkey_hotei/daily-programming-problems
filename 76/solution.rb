#!/usr/bin/ruby

# Problem: Given a 2D n by m table of letters, determine how many columns would need to
#          would need to be deleted s.t. for each row, each column is sorted lexicographically;

def count_column_deletes(table)
  count = 0
  num_cols = table.first.length
  (0..num_cols - 1).each do |col_index|
    (1..table.length - 1).each do |row_index|
      last_letter = table[row_index - 1][col_index]
      this_letter = table[row_index][col_index]

      if this_letter < last_letter
        count += 1
      end
    end
  end
  count
end


def main
  table = ["abc", "daf", "ghi"]
  puts count_column_deletes(table)  # => 1

  table = ["abcdef"]
  puts count_column_deletes(table)  # => 0

  table = ["zyx", "wvu", "tsr"]
  puts count_column_deletes(table)  # => 3
end

main
