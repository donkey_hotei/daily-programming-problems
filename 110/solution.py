#!/usr/bin/env python

# Given a binary tree, return all paths from the root to the leaves.


class Stack(object):
    def __init__(self):
        self.items = []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def top(self):
        return self.items[-1]

    def is_empty(self):
        return len(self.items) == 0


class Node(object):
    def __init__(self, value=None, left=None, right=None):
        self.value, self.left, self.right = value, left, right

    def children(self):
        return filter(None, [self.left, self.right])


def build_path(node, parent):
    s = Stack()
    while node is not None:
        s.push(node)
        node = parent[node]
    return map(lambda n: n.value, s.items[::-1])


def enumerate_paths(root):
    if root is None:
        return

    paths = []
    stack = Stack()
    stack.push(root)
    parent = {root: None}

    # iterative pre-order traversal
    while not stack.is_empty():
        node = stack.pop()
        if len(node.children()) == 0:
            paths.append(build_path(node, parent))
        for child in node.children():
            parent[child] = node
            stack.push(child)

    return paths


def tests():
    #       1
    #      / \
    #     2   3
    #        / \
    #       4   5
    tree = Node(1,
                Node(2),
                Node(3,
                     Node(4),
                     Node(5)
                     )
                )

    assert enumerate_paths(tree) == [[1, 3, 5], [1, 3, 4], [1, 2]]
    print "tests pass"


if __name__ == "__main__":
    print(tests())
