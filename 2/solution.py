#!/usr/bin/python

# Given an array of integers, return a new array s.t. each element at index i of the
# new array is the product of all the numbers in the original array except the one
# at i. Solve without using division and in linear time.

def products(nums):
    pp = []  # prefix products
    for n in nums:
        if pp:
            pp.append(pp[-1] * n)
        else:
            pp.append(n)

    sp = [] # suffix products
    for n in reversed(nums):
        if sp:
            sp.append(sp[-1] * n)
        else:
            sp.append(n)

    sp = list(reversed(sp))

    result = []
    for i in range(len(nums)):
        if i == 0:
            result.append(sp[i + 1])
        elif i == len(nums) - 1:
            result.append(pp[i - 1])
        else:
            result.append(pp[i - 1] * sp[i + 1])

    return result


if __name__ == "__main__":
    a = [1, 2, 3, 4, 5]
    print products(a)
