#!/usr/bin/env ruby

#
# Given a binary tree, find the lowest common ancestor of two given
# nodes in the tree. Assume that each node in the tree has a pointer
# to it's parent.
#

class Node
  attr_accessor :value, :left, :right

  def initialize(value=nil, left=nil, right=nil)
    @value, @left, @right = value, left, right
  end

  def children
    [@left, @right].compact
  end

  def lca(n1, n2)
    if (right && value < n1 && value < n2)
      right.lca(n1, n2)
    elsif (left && value > n1 && value > n2)
      left.lca(n1, n2)
    else
      return self
    end
  end
end


def main
  #       1
  #      / \
  #     2   3
  #        / \
  #       4   5
  tree = Node.new(1,
                  Node.new(2),
                  Node.new(3,
                           Node.new(4),
                           Node.new(5)))
  puts tree.lca(4, 5).value
end

main