#!/usr/bin/python
import string
from stack import Stack
# Problem:
#   Suppose an arithmetic expression is given as a tree. Each leaf is an integer and
#   each internal node is one of `+`, `-`, `/`, or `*`.
#   Given the root to such a tree, write a function to evaluate it.
#
#

class Node:
    """
    Single node of a binary tree.
    """
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left  = left
        self.right = right

def is_operator(s):
    return s in ["+", "/", "-", "-"]

def is_digit(s):
    return str(s) in string.digits

def evaluate(node):
    if (node.value is not None and is_digit(node.value)):
        return node.value
    elif (is_operator(node.value)):
        left     = evaluate(node.left)
        right    = evaluate(node.right)
        operator = node.value
        result   = eval("{} {} {}".format(left, operator, right))

        node.value = result
        return result

if __name__ == "__main__":
    # (3 + 2) / 5
    tree = Node(value="/",
                left=Node(value=5),
                right=Node(value="+",
                           left=Node(value=3),
                           right=Node(value=2)))
    print evaluate(tree)

