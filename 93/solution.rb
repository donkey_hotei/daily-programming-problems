#!/usr/bin/ruby

class Node
  attr_accessor :data, :left, :right

  def initialize(data=nil, left=nil, right=nil)
    @data, @left, @right = data, left, right
  end

  def children
    [@left, @right].compact
  end

  def height
    return 0 if children.empty?
    children.collect { |child| child.height + 1 }.max
  end
end

def bst?(root, min=nil, max=nil)
  return true if root.nil?

  if (!min.nil? && min >= root.data) || (!max.nil? && max <= root.data)
    return false
  end

  if !bst?(root.left, min, root.data) || !bst?(root.right, root.data, max)
    return false
  end

  return true
end

def subtree_bst(tree)
  return tree if bst?(tree) || tree.nil?

  l_subtree = tree.left
  r_subtree = tree.right

  if subtree_bst(l_subtree) && subtree_bst(r_subtree)
    return [l_subtree, r_subtree].max_by { |t| t.height }
  elsif subtree_bst(l_subtree)
    return l_substree
  elsif subtree_bst(r_subtree)
    return r_subtree
  end
end


def main
  tree = Node.new(data=10,
                  Node.new(data=5,
                           left=Node.new(1),
                           right=Node.new(8)),
                  Node.new(data=15,
                           right=Node.new(7)))
  puts subtree_bst(tree)
end

main