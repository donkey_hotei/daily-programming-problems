#!/usr/bin/ruby
# https://stackoverflow.com/questions/6313308/get-all-the-diagonals-in-a-matrix-list-of-lists-in-python#6313407

class ChessBoard
  attr_accessor :bishops, :height, :width

  def initialize(bishops, m)
    @bishops = bishops
    @height  = m
    @width   = m
  end

  def count_bishop_conflicts
    conflicts = 0
    diagonals.each do |diagonal|
      bishop_count = 
        (diagonal.select { |e| bishops.include?(e) }).count
      if bishop_count > 1
        # how many pairs are there in a row?
        loop do
          break unless bishop_count % 2 == 0
          bishop_count /= 2
          conflicts += 1
        end
      end
    end
    conflicts
  end

  def diagonals
    # we go from (x, y) to (p, q) where p is the number
    # of the diagonal and q is the index of the diagonal
    # using two simple transformations:
    #               x = q, y = p - q
    #                    and
    #           x = N - 1 - q, y = p - q
    #
    @diagonals ||=
      (0..height + width - 1).map do |p|
        [p, height - 1].min
          .downto([0, p - width + 1].max).map do |q|
            [q, p - q]
          end
      end.concat(
      # flip matrix left-right
      (0..height + width - 1).map do |p|
        [p, height - 1].min
          .downto([0, p - width + 1].max).map do |q|
            [height - 1 - q, p - q]
          end
      end
      )
  end
end

def main
  board = ChessBoard.new(
    [[0, 0], [1, 2], [2, 2], [4, 0]], 5
  )
  puts board.count_bishop_conflicts
  # puts board.diagonals.inspect
end

main