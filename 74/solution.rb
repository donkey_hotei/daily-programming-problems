#!/user/bin/ruby

# Problem: Given two integers n and x, write the number of times the number
#          x appears in an n by n multiplication table.
#

class MultiplicationTable
  attr_accessor :n

  def initialize(n)
    self.n = n
  end

  #
  # The trick here is to exploit the fact that
  # there will be an occurance of x for every
  # factor of x that is inside of the matrix.
  def count(x)
    (1..n + 1).map do |i|
      if x % i == 0 && x / i <= n
        1
      end
    end.compact.reduce(:+)
  end
end


def main
  table = MultiplicationTable.new(6)
  puts table.count(12)
end

main