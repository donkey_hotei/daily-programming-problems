package main

/*
 * Problem: implement a function pow(x, n) that exponentiates
 *          a value x by n. implement something faster than
 *          the naive approach of iterative multiplication.
 *
 * Since the multiplication step takes about the same amount of
 * time no matter the size of the numbers being multiplied,
 * we look to move the work being done in the exponent into the
 * base. We can do this by exploiting a well-known property
 * of exponentiation. Recall that:
 *
 * If x is even then x^n is the same as x * (x^2)^((n-1)/2),
 * and if x is odd then x^n is the same as (x^2)^(n/2)
 *
 * This property allows ones to use the bits in the exponent
 * to determine which powers are computed.
 */

import (
    "fmt"
)

/*
 * integer exponentiation by successive squaring!
 * https://en.wikipedia.org/wiki/Exponentiation_by_squaring
 */
func exp(x, n int) int {
    if n < 0 {
        x = 1 / x
        n = -n
    }
    if n == 0 {
        return 1
    }

    y := 1

    for {
        if n < 1 {
            break
        }
        if n%2 == 0 {
            x = x * x
            n = n / 2
        } else {
            y = x * y
            x = x * x
            n = (n - 1) / 2
        }
    }

    return y
}

func main() {
    fmt.Printf("%d\n", exp(2, 10))
}
