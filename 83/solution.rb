#!/usr/bin/ruby

class Node
  attr_reader :value

  def initialize(value, left=nil, right=nil)
    @value, @left, @right = value, left, right
  end

  def children
    [@left, @right].compact
  end

  def invert
    return if children.empty?

    @left.invert unless @left.nil?
    @right.invert unless @right.nil?

    @left, @right = @right, @left
  end

  def to_s
    self.inspect
  end
end

def main
  tree = Node.new("A",
                  Node.new("B",
                          Node.new("D"),
                          Node.new("E")
                  ),
                  Node.new("C",
                           Node.new("F")
                          )
                  )
  puts tree.invert
end

main